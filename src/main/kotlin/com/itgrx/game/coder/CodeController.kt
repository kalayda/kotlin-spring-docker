package com.itgrx.game.coder

import com.itgrx.game.data.coder.Coder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.math.BigInteger
import java.util.*

@RestController
class CodeController {

    @GetMapping("/uid")
    fun uid(
            @RequestParam(value = "n", defaultValue = "1") n: Int
    ): Collection<Long> {
        val res = mutableListOf<Long>()
        for (i in 1..n)
            res += Coder().uid()
        return res
    }

    @GetMapping("/code")
    fun code(
            @RequestParam(value = "uid") uid: String,
            @RequestParam(value = "clan") clan: Int,
            @RequestParam(value = "race") race: Int
    ) =
            Base64.getEncoder().withoutPadding().encode(Coder().encode(BigInteger(uid, 16).toLong(), null, clan, race))

}